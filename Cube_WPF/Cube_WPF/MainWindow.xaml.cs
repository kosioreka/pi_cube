﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Windows.Markup;
using HelixToolkit.Wpf;
using Cube_WPF.Utilities.Algorithms;
using Cube_WPF.Utilities.Viewer;
using Cube_WPF.Utilities;

namespace Cube_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region private fields
        private Point _previousPosition2D;
        private Vector3D _previousPosition3D = new Vector3D(0, 0, 1);
        private Transform3DGroup _transform;
        private AxisAngleRotation3D _rotation = new AxisAngleRotation3D();

        private Cube cube;
        private Stack<LinesVisual3D> paths = new Stack<LinesVisual3D>();
        private Graph graph;
        private NetsManager netsManager;
        private LightManager lightManager = new LightManager();
        private GeometryModel3D mGeometry;
        private bool _canMarkEdges;
        private int NetIndex;
        #endregion

        /// <summary>
        /// Indicates if user can select edges
        /// True when the net is not displayed
        /// </summary>
        public bool CanMarkEdges
        {
            get { return _canMarkEdges; }
            set
            {
                _canMarkEdges = value;
                revertMenuItem.IsEnabled = _canMarkEdges;
            }
        }
        /// <summary>
        /// For window initialization
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            generateCube();

            _transform = new Transform3DGroup();
            _transform.Children.Add(new RotateTransform3D(_rotation));
            viewPort.Camera.Transform = _transform;

            CanMarkEdges = true;
        }

        #region nets generating
        /// <summary>
        /// Looks for a porper net
        /// </summary>
        private void FindNet()
        {
            graph.setNeighburs();
            graph.calculateLongestPath();
            NetIndex = GraphChecker.getNetOfTheGraph(graph, netsManager);
            GenerateNets();
        }
        /// <summary>
        /// Generates the net on the screen
        /// </summary>
        private void GenerateNets()
        {
            Reset();
            var netFaces = netsManager.netsViewers[NetIndex].getNetFaces();
            Group.Children.Clear();
            foreach (var wall in netFaces)
            {
                Group.Children.Add(new GeometryModel3D()
                {
                    Geometry = new MeshGeometry3D()
                    {
                        Positions = wall.Positions,
                        TriangleIndices = wall.Indices
                    },
                    Material = new DiffuseMaterial(wall.Color),
                    BackMaterial = new DiffuseMaterial(wall.Color)

                });
            }
            updateLight();
        }
        /// <summary>
        /// Hit Test Method for selecting an edge
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private HitTestResultBehavior HitTestResultCallback(HitTestResult result)
        {
            // Did we hit 3D?
            RayHitTestResult rayResult = result as RayHitTestResult;
            if (rayResult != null)
            {
                // Did we hit a MeshGeometry3D?
                RayMeshGeometry3DHitTestResult rayMeshResult =
                    rayResult as RayMeshGeometry3DHitTestResult;

                if (rayMeshResult != null)
                {
                    // Yes we did!
                    Point3D p = rayMeshResult.PointHit;

                    int v = rayMeshResult.VertexWeight1 > rayMeshResult.VertexWeight3 ? rayMeshResult.VertexIndex1 : rayMeshResult.VertexIndex3;
                    int u = rayMeshResult.VertexIndex2;

                    Vector3D v3D = Trackball.VectorFromPoint(rayMeshResult.MeshHit.Positions[v]);
                    Vector3D u3D = Trackball.VectorFromPoint(rayMeshResult.MeshHit.Positions[u]);
                    Vector3D p3D = Trackball.VectorFromPoint(p);
                    double d = Vector3D.CrossProduct(p3D - v3D, p3D - u3D).Length / (v3D - u3D).Length;
                    if (d < 0.1D)
                    {
                        IsProperEdgeSelected( v,  u, rayMeshResult);
                    }
                }
            }

            return HitTestResultBehavior.Continue;
        }
        /// <summary>
        /// Checks all requirements while selecting an edge
        /// </summary>
        /// <param name="v"></param>
        /// <param name="u"></param>
        /// <param name="rayMeshResult"></param>
        private void IsProperEdgeSelected(int v, int u, RayMeshGeometry3DHitTestResult rayMeshResult)
        {
            if (!cube.edges[cube.findEdge(v, u)].marked)
            {
                int index = cube.findEdge(v, u);
                if (cube.nrMarkedEdges <= 7)
                {
                    if (graph.RemoveEgde(index, cube.nrMarkedEdges == 7))
                    {
                        if (!cube.markEdge(index))
                        {
                            throw new Exception("Marking cube went wrong!");
                        }
                        SelectEdge(rayMeshResult.MeshHit.Positions[v], rayMeshResult.MeshHit.Positions[u]);
                        infoBox.Text = "Liczba zaznaczonych krawędzi: " + cube.nrMarkedEdges;
                    }
                    else
                    {
                        infoBox.Text = "Niepoprawna krawędź!  Liczba zaznaczonych krawędzi: " + cube.nrMarkedEdges;
                    }
                    if (cube.nrMarkedEdges == 7)
                    {
                        FindNet();
                        CanMarkEdges = false;
                        infoBox.Text = "Utworzono siatkę! W celu wygenerowania nowej, naciśnij resetuj";
                    }
                }
                else
                {
                    infoBox.Text = "Nie można więcej zaznaczać krawędzi!";
                }
            }
        }
        #endregion

        #region cubeHistoryChanges
        /// <summary>
        /// Visualization of selecting an edge
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        private void SelectEdge(Point3D p1, Point3D p2)
        {
            Point3DCollection pointcollect = new Point3DCollection();
            pointcollect.Add(p1);
            pointcollect.Add(p2);
            paths.Push(new LinesVisual3D()
            {
                Thickness = 5,
                Points = pointcollect,
                Color = Color.FromRgb(219, 112, 147)
            });
            viewPort.Children.Add(paths.Peek());
        }
        /// <summary>
        /// Deselect the latest selected edge
        /// </summary>
        private void DeselectEdge()
        {
            int index;
            if ((index = cube.revert()) != -1)
            {
                graph.revert();
                viewPort.Children.Remove(paths.Pop());
                infoBox.Text = "Liczba zaznaczonych krawędzi: " + cube.nrMarkedEdges;
            }
            else
            {
                //TODO: info no more to deselect!!
            }
        }
        /// <summary>
        /// Reset the history
        /// Creates new cube
        /// </summary>
        private void Reset()
        {
            cube.reset();
            graph = new Graph(cube);
            while (paths.Count > 0)
            {
                viewPort.Children.Remove(paths.Pop());
            }
            paths.Clear();
            if (CanMarkEdges)
            {
                infoBox.Text = "Liczba zaznaczonych krawędzi: " + cube.nrMarkedEdges;
            }
        }
        /// <summary>
        /// Resets the cube on changes
        /// </summary>
        private void resetCube()
        {
            Group.Children.Clear();

            MeshGeometry3D mesh = new MeshGeometry3D();
            // Front face
            mesh.Positions.Add(new Point3D(-0.5, -0.5, -0.5));
            mesh.Positions.Add(new Point3D(0.5, -0.5, -0.5));
            mesh.Positions.Add(new Point3D(-0.5, 0.5, -0.5));
            mesh.Positions.Add(new Point3D(0.5, 0.5, -0.5));
            // Back face
            mesh.Positions.Add(new Point3D(-0.5, -0.5, 0.5));
            mesh.Positions.Add(new Point3D(0.5, -0.5, 0.5));
            mesh.Positions.Add(new Point3D(-0.5, 0.5, 0.5));
            mesh.Positions.Add(new Point3D(0.5, 0.5, 0.5));

            mesh.TriangleIndices = new Int32Collection() { 2, 3, 1, 1, 0, 2, 1, 3, 7, 7, 5, 1, 5, 7, 6, 6, 4, 5, 6, 2, 0, 0, 4, 6, 7, 3, 2, 2, 6, 7, 0, 1, 5, 5, 4, 0 };

            mGeometry = new GeometryModel3D(mesh, new DiffuseMaterial(ViewerManager.cubeColor));
            Group.Children.Add(mGeometry);

            updateLight();
        }


        /// <summary>
        /// Generates the whole new cube
        /// Used upon starting programm or reseting the program after the net is displayed
        /// </summary>
        private void generateCube()
        {
            ViewerManager.cubeColor = Brushes.Red;
            Trackball._rotationFactor = 3.0;
            resetCube();

            cube = new Cube();
            cube.GenerateNewCube();
            graph = new Graph(cube);
            netsManager = new NetsManager(cube);
        }

        #endregion

        #region update methods
        /// <summary>
        /// Updates the color of the cube based on user choice
        /// </summary>
        /// <param name="color"></param>
        private void updateColor(Brush color)
        {

            ViewerManager.cubeColor = color;
            mGeometry.Material = new DiffuseMaterial(color);

            if (!CanMarkEdges)
            {
                GenerateNets();
            }
        }
        /// <summary>
        /// Updates the light of the cube based on user choice
        /// </summary>
        private void updateLight()
        {
            foreach (var light in lightManager.getLights())
                Group.Children.Add(light);
        }
        /// <summary>
        /// Opens help dialog
        /// </summary>
        private void helpDialog()
        {
            string messageBoxText = "Możliwe funkcje myszy:" + Environment.NewLine +
                "prawy - zaznaczenie krawędzi" + Environment.NewLine +
                "lewy - obroty modelu" + Environment.NewLine +
                "scroll - przblliżenie i oddalenie modelu";
            MessageBox.Show(messageBoxText, "Instrukcja programu");
        }
        #endregion

        #region interactions
        /// <summary>
        /// Zooming with the wheel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseWheel(object sender, MouseWheelEventArgs e)
        {

            camera.Position = new Point3D(
                camera.Position.X - e.Delta / 1000D,
                camera.Position.Y - e.Delta / 1000D,
                camera.Position.Z - e.Delta / 1000D);
            
        }
        /// <summary>
        /// Selecting edge mouse click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            //MessageBox.Show("Here!");
            if (CanMarkEdges)
            {
                Point pt = e.GetPosition(viewPort);
                PointHitTestParameters hitParams = new PointHitTestParameters(pt);
                VisualTreeHelper.HitTest(viewPort, null, HitTestResultCallback, hitParams);
            }
        }
        /// <summary>
        /// User menu selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            switch (((MenuItem)sender).Name)
            {
                case "revertMenuItem":
                    DeselectEdge();
                    break;
                case "resetMenuItem":
                    Reset();
                    if (!CanMarkEdges)
                    {
                        CanMarkEdges = true;
                        resetCube();
                    }
                    break;
                case "colorRedMenuItem":
                    updateColor(Brushes.Red);
                    break;
                case "colorGreenMenuItem":
                    updateColor(Brushes.Green);
                    break;
                case "colorBlueMenuItem":
                    updateColor(Brushes.Blue);
                    break;
                case "colorYellowtMenuItem":
                    updateColor(Brushes.Yellow);
                    break;
                case "lightAmbientMenuItem":
                    lightManager.Type = LightType.AmbientLight;
                    if (CanMarkEdges)
                        resetCube();
                    else GenerateNets();
                    break;
                case "lightMultiMenuItem":
                    lightManager.Type = LightType.Multilight;
                    if (CanMarkEdges)
                        resetCube();
                    else GenerateNets();
                    break;
                case "lightDirectionalMenuItem":
                    lightManager.Type = LightType.DirectionalLight;
                    if (CanMarkEdges)
                        resetCube();
                    else GenerateNets();
                    break;
                case "helpMenuItem":
                    helpDialog();
                    break;
            }
        }
        #endregion

        #region rotation

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //MessageBox.Show("Here2!");
            Mouse.Capture(myElement, CaptureMode.Element);

            _previousPosition2D = e.GetPosition(viewPort);
            _previousPosition3D = Trackball.ProjectToTrackball(
                viewPort.ActualWidth,
                viewPort.ActualHeight,
                _previousPosition2D);

        }

        private void Window_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Mouse.Capture(myElement, CaptureMode.None);
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            Point currentPosition = e.GetPosition(viewPort);

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Trackball.Track(currentPosition, viewPort, ref _previousPosition3D, ref _rotation);
            }
            _previousPosition2D = currentPosition;
        }

        #endregion

    }
}
