﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cube_WPF
{
    /// <summary>
    /// Logical visualisation of the Cube
    /// </summary>
    public class Cube
    {
        /// <summary>
        /// Edges of the cube
        /// </summary>
        public List<Edge> edges = new List<Edge>();
        /// <summary>
        /// Amount of marked edges
        /// Supports user's egde cutting
        /// </summary>
        public int nrMarkedEdges = 0;
        /// <summary>
        /// Faces of the cube
        /// </summary>
        public List<Face> faces = new List<Face>();
        /// <summary>
        /// Unmarks all edges
        /// </summary>
        public void reset()
        {
            nrMarkedEdges = 0;
            History.reset();
            foreach(var e in edges)
            {
                e.marked = false;
            }
        }
        /// <summary>
        /// Reverts the last removed edge
        /// </summary>
        /// <returns></returns>
        public int revert()
        {
            int removeIndex = -1;
            if (nrMarkedEdges > 0)
            {
                nrMarkedEdges--;
                removeIndex = History.removeEdge();
                edges[removeIndex].marked = false;
            }
            return removeIndex;
        }
        /// <summary>
        /// Marks the edge of the cube and adds this information to the history
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool markEdge(int index)
        {
            if (index != -1)
            {
                if (!edges[index].marked)//just a precoution!
                {               
                    edges[index].marked = true;
                    nrMarkedEdges++;
                    History.addEdge(index);
                    return true;
                }
                else
                {
                    throw new Exception("Wrong implematation");
                }
            }
            return false;
        }
        /// <summary>
        /// Finds the edge upon the vertices
        /// The order of vertices does not matter
        /// </summary>
        /// <param name="v"></param>
        /// <param name="u"></param>
        /// <returns></returns>
        public int findEdge(int v, int u)
        {
            if (v > u) {
                return findEdge(u, v);
            }
            for (int i = 0;i<edges.Count;i++)
            {
                if (v == edges[i].v && u == edges[i].u)
                    return i;
            }           
            return -1;
        }
        /// <summary>
        /// Generates new cube
        /// Used upon starting programm or reseting the program after the net is displayed
        /// </summary>
        public void GenerateNewCube()
        {
            this.edges.Add(new Edge(0, 1));
            this.edges.Add(new Edge(0, 2));
            this.edges.Add(new Edge(1, 3));
            this.edges.Add(new Edge(2, 3));
            this.edges.Add(new Edge(0, 4));
            this.edges.Add(new Edge(3, 7));
            this.edges.Add(new Edge(1, 5));
            this.edges.Add(new Edge(2, 6));
            this.edges.Add(new Edge(4, 6));
            this.edges.Add(new Edge(5, 7));
            this.edges.Add(new Edge(6, 7));
            this.edges.Add(new Edge(4, 5));

            this.faces.Add(new Face(this, 0, 1, 2, 3, Sphere.X, Sphere.Z));
            this.faces.Add(new Face(this, 0, 1, 4, 5, Sphere.Y, Sphere.Z));
            this.faces.Add(new Face(this, 0, 2, 4, 6, Sphere.X, Sphere.Y));
            this.faces.Add(new Face(this, 2, 3, 6, 7, Sphere.Y, Sphere.Z));
            this.faces.Add(new Face(this, 1, 3, 5, 7, Sphere.X, Sphere.Y));
            this.faces.Add(new Face(this, 4, 5, 6, 7, Sphere.X, Sphere.Z));
        }
    }

    /// <summary>
    /// Edge of a cube: two vertices (indexes)
    /// </summary>
    public class Edge
    {
        public int v;
        public int u;
        /// <summary>
        /// Indicates whether the edge is marked or not
        /// </summary>
        public bool marked { get; set; }

        public Edge(int v, int u)
        {
            this.v = v;
            this.u = u;
            this.marked = false;
        }
    }

    /// <summary>
    /// Sphere of the cube
    /// Every face of the cube is perpendicular to two spheres
    /// </summary>
    public enum Sphere
    {
        X = 0, Y =1, Z=2
    }
}
