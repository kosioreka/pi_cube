﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cube_WPF
{
    /// <summary>
    /// Face of the cube
    /// Contains 4 edges
    /// </summary>
    public class Face
    {
        /// <summary>
        /// List of 4 edges surrounding the face
        /// </summary>
        public List<Edge> edges = new List<Edge>();

        /// <summary>
        /// Neighbouring faces
        /// </summary>
        public List<int> neighbours{ get; set; }
        /// <summary>
        /// Indicates if the face is a leaf due to the cutted edges
        /// </summary>
        public bool isLeaf { get; set;}

        /// <summary>
        /// Special marker for algorithms
        /// </summary>
        public bool visited { get; set; }
        /// <summary>
        /// First sphere that the face is perpendicular to
        /// </summary>
        public Sphere sphere1;
        /// <summary>
        /// Second sphere that the face is perpendicular to
        /// </summary>
        public Sphere sphere2;
        /// <summary>
        /// Returns whether face is perpendicular to the X sphere
        /// </summary>
        /// <returns></returns>
        public bool isXSphere()
        {
            return sphere1 == Sphere.X || sphere2 == Sphere.X;
        }
        /// <summary>
        /// Returns whether face is perpendicular to the Y sphere
        /// </summary>
        /// <returns></returns>
        public bool isYSphere()
        {
            return sphere1 == Sphere.Y || sphere2 == Sphere.Y;
        }
        /// <summary>
        /// Returns whether face is perpendicular to the Z sphere
        /// </summary>
        /// <returns></returns>
        public bool isZSphere()
        {
            return sphere1 == Sphere.Z || sphere2 == Sphere.Z;
        }


        /// <summary>
        /// Constractor that generates edges list of the face
        /// 4 indexes of vertices of the cube that create a face of the cube
        /// Checks which vertices are connected and create an edge.
        /// </summary>
        public Face(Cube c, int v, int u, int x, int y, Sphere s1, Sphere s2)
        {
            int index = c.findEdge(v, u);
            if (index != -1) edges.Add(c.edges[index]);

            index = c.findEdge(x, u);
            if (index != -1) edges.Add(c.edges[index]);

            index = c.findEdge(y, u);
            if (index != -1) edges.Add(c.edges[index]);

            index = c.findEdge(v, x);
            if (index != -1) edges.Add(c.edges[index]);

            index = c.findEdge(v, y);
            if (index != -1) edges.Add(c.edges[index]);

            index = c.findEdge(y, x);
            if (index != -1) edges.Add(c.edges[index]);

            if (edges.Count != 4) throw new IndexOutOfRangeException();

            sphere1 = s1;
            sphere2 = s2;
        }

        
    }
}
