﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cube_WPF.Utilities.Algorithms;

namespace Cube_WPF
{
    /// <summary>
    /// A Graph implementation of the cube
    /// Vertex of the graph is a face of the cube
    /// </summary>
    public class Graph
    {
        /// <summary>
        /// Edges of the graph
        /// </summary>
        public List<EdgeGraph> edgeGraphs = new List<EdgeGraph>();
        /// <summary>
        /// Faces of the cube - used for easier implemenation
        /// </summary>
        public List<Face> faceGraphs = new List<Face>();
        /// <summary>
        /// Amount of faces
        /// </summary>
        private int N;

        /// <summary>
        /// Longest path in the X sphere
        /// </summary>
        public int maxX;
        /// <summary>
        /// Longest path in the Y sphere
        /// </summary>
        public int maxY;
        /// <summary>
        /// Longest path in the Z sphere
        /// </summary>
        public int maxZ;
        /// <summary>
        /// Amount of leaves
        /// </summary>
        public int leavesAmount;

        /// <summary>
        /// Create a graph based on a cube.
        /// Vertex of the graph is a face of the cube
        /// </summary>
        /// <param name="cube"></param>
        public Graph(Cube cube)
        {
            this.faceGraphs = cube.faces;
            N = cube.faces.Count;
            for(int i = 0; i < faceGraphs.Count-1; i++)
            {
                for(int j = i+1; j < faceGraphs.Count; j++)
                {
                    foreach(var v in faceGraphs[i].edges)
                    {
                        foreach(var u in faceGraphs[j].edges)
                        {
                            if (v.v == u.v && v.u == u.u)
                            {                                
                                edgeGraphs.Add(new EdgeGraph() { A = i, B=j, e = cube.edges.IndexOf(v) });
                                goto PETLA;
                            }
                        }
                    }
                PETLA:;
                }
            }
        }
        /// <summary>
        /// Default constractor
        /// </summary>
        public Graph()
        {
            N = 6;
        }
        /// <summary>
        /// Checks if after remiving an edge the net can be still generated
        /// 
        /// </summary>
        /// <param name="edgeIndex"></param>
        /// <param name="last"> On the 7th marked edge the graph but be not cyclic </param>
        /// <returns></returns>
        public bool RemoveEgde(int edgeIndex, bool last)
        {
            var eG = edgeGraphs.FirstOrDefault(e => e.e == edgeIndex);
            List<EdgeGraph> removedList = new List<EdgeGraph>(edgeGraphs);
            removedList.Remove(eG);
            if (last)
            {
                if (!GraphChecker.isNotCyclic(removedList, N))
                    return false;
            }
            if (GraphChecker.Connectivity(removedList, N))
            {
                edgeGraphs.Remove(eG);
                History.addEdgeGraph(eG);
                return true;
            }
            return false;
        }
        /// <summary>
        /// Reverts the last selected edge
        /// </summary>
        public void revert()
        {
            edgeGraphs.Add(History.removeEdgeGraph());
        }
       /// <summary>
       /// Returns the index of the edge that connects two faces: a and b
       /// In graph visualization: returns the index of the edge between two vertices: a and b
       /// </summary>
       /// <param name="a"></param>
       /// <param name="b"></param>
       /// <returns></returns>
        public int getIndexEdge(int a, int b)
        {
            int index=0;
            index = edgeGraphs.FirstOrDefault(eG => (eG.A == a && eG.B == b) || (eG.B == a && eG.A == b)).e;
            return index;
        }

        /// <summary>
        /// Sets the neighbours for each face of the cube
        /// Supports looking for a proper net
        /// </summary>
        public void setNeighburs()
        {    
            for(int i = 0; i< faceGraphs.Count; i++)
            {
                faceGraphs[i].neighbours = new List<int>();
                var faces = edgeGraphs.FindAll(eG => eG.A == i || eG.B == i);
                foreach(var f in faces)
                {
                    faceGraphs[i].neighbours.Add(f.B == i ? f.A : f.B);
                }
                if(faces.Count() == 1)
                {
                    faceGraphs[i].isLeaf = true;
                    leavesAmount++;
                }
            }
        }
        /// <summary>
        /// Calculates longest paths in all spheres
        /// Supports looking for a proper net
        /// </summary>
        public void calculateLongestPath()
        {
            maxX = maxY = maxZ = 0;
            var stackFaces = new Stack<Face>();
            var stackX = new Stack<int>();
            var stackY = new Stack<int>();
            var stackZ = new Stack<int>();
            var startFaces = faceGraphs.Where(f => f.isLeaf == true);

            foreach(var startFace in startFaces)
            {
                int tempX = 0, tempY = 0, tempZ = 0;
                stackFaces.Push(startFace);
                stackX.Push(tempX);
                stackY.Push(tempY);
                stackZ.Push(tempZ);

                while (stackFaces.Count != 0)
                {
                    var tempFace = stackFaces.Pop();
                    tempFace.visited = true;
                    tempX = stackX.Pop();
                    tempY = stackY.Pop();
                    tempZ = stackZ.Pop();

                    if (tempFace.isXSphere())
                        tempX++;
                    else
                    {
                        maxX = (maxX < tempX ? tempX : maxX);
                        tempX = 0;
                    }
                    if (tempFace.isYSphere())
                        tempY++;
                    else
                    {
                        maxY = (maxY < tempY ? tempY : maxY);
                        tempY = 0;
                    }
                    if (tempFace.isZSphere())
                        tempZ++;
                    else
                    {
                        maxZ = (maxZ < tempZ ? tempZ : maxZ);
                        tempZ = 0;
                    }
                    foreach(var nIndex in tempFace.neighbours)
                    {
                        if (!faceGraphs[nIndex].visited)
                        {
                            stackFaces.Push(faceGraphs[nIndex]);
                            stackX.Push(tempX);
                            stackY.Push(tempY);
                            stackZ.Push(tempZ);
                        }
                    }
                    maxX = (maxX < tempX ? tempX : maxX);
                    maxY = (maxY < tempY ? tempY : maxY);
                    maxZ = (maxZ < tempZ ? tempZ : maxZ);
                }
                faceGraphs.ForEach(f => f.visited = false);
            }

        }     
        /// <summary>
        /// Returns the longest path from all the spheres
        /// </summary>
        /// <returns></returns>
        public int getLongestPath()
        {
            return Math.Max(Math.Max(maxX, maxY), maxZ);
        }
        /// <summary>
        /// Returns the shortest path from all the spheres
        /// </summary>
        /// <returns></returns>
        public int getShortestPath()
        {
            return Math.Min(Math.Min(maxX, maxY), maxZ);
        }

    }

    /// <summary>
    /// A represenation of an edge in a graph
    /// Vertex of the graph is a face of the cube
    /// Edge between them represents an edge that connect two faces
    /// </summary>
    public class EdgeGraph
    {
        /// <summary>
        /// first vertex of edge = first face of the cube
        /// </summary>
        public int A;
        /// <summary>
        /// second vertex of edge = second face of the cube
        /// </summary>
        public int B;
        /// <summary>
        /// index of an egde connecting two faces: A and B
        /// </summary>
        public int e; 
        /// <summary>
        /// Constractor
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="_e"></param>
        public EdgeGraph(int a, int b, int _e)
        {
            A = a;
            B = b;
            e = _e;
        }
        /// <summary>
        /// Empty constractor
        /// </summary>
        public EdgeGraph() { }
        /// <summary>
        /// Returns the neighbour of the face v
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public int getNeighbour(int v)
        {
            return B == v ? A : B; 
        }
    }
}
