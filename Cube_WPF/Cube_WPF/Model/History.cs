﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cube_WPF
{
    /// <summary>
    /// Static class storing the history of the user operations
    /// </summary>
    public static class History
    {
        /// <summary>
        /// stack for esdges of the cube
        /// </summary>
        private static Stack<int> edgesOrder = new Stack<int>();
        /// <summary>
        /// stack for edges of the graph
        /// </summary>
        private static Stack<EdgeGraph> edgeGraphsOrder = new Stack<EdgeGraph>();
        /// <summary>
        /// Adds the history index of the selected edge
        /// </summary>
        /// <param name="index"></param>
        public static void addEdge(int index)
        {
            edgesOrder.Push(index);
        }
        /// <summary>
        /// Removes from the history index of the selected edge
        /// </summary>
        /// <returns></returns>
        public static int removeEdge()
        {
            return edgesOrder.Pop();
        }
        /// <summary>
        /// Adds the history selected edge of the graph
        /// </summary>
        /// <param name="eG"></param>
        public static void addEdgeGraph(EdgeGraph eG)
        {
            edgeGraphsOrder.Push(eG);
        }
        /// <summary>
        /// Removes from the history selected edge of the graph
        /// </summary>
        /// <returns></returns>
        public static EdgeGraph removeEdgeGraph()
        {
            return edgeGraphsOrder.Pop();
        }
        /// <summary>
        /// Resets the history
        /// </summary>
        public static void reset()
        {
            edgesOrder.Clear();
            edgeGraphsOrder.Clear();
        }
    }
}