﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cube_WPF.Utilities.Algorithms
{
    /// <summary>
    /// Contains algorithms used for proving if the graph represents a proper net of the cube
    /// </summary>
    public static class GraphChecker
    {
        /// <summary>
        /// Checks if the graph is connected
        /// uses DFS algorithm
        /// </summary>
        /// <returns></returns>
        public static bool Connectivity(List<EdgeGraph> edges, int N)
        {
            bool[] visited = new bool[N];
            int vc = 0;
            Stack<int> S = new Stack<int>();
            S.Push(0);
            visited[0] = true;
            while (S.Count > 0)
            {
                int v = S.Pop();
                vc++;
                foreach (var eG in edges.Where(a => a.A == v || a.B == v))
                {
                    int u = eG.getNeighbour(v);
                    if (visited[u])
                        continue;
                    visited[u] = true;
                    S.Push(u);
                }
            }
            if (vc == N)
                return true;
            return false;
        }
        /// <summary>
        /// Checks if the graph is not cyclic
        /// uses DFS algorithm
        /// </summary>
        /// <param name="edges"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public static bool isNotCyclic(List<EdgeGraph> edges, int N)
        {
            bool[] visited = new bool[N];
            int vc = 0;
            Stack<int> S = new Stack<int>();
            S.Push(0);
            S.Push(-1);
            visited[0] = true;
            while (S.Count > 0)
            {
                int w = S.Pop();
                int v = S.Pop();
                vc++;
                foreach (var eG in edges.Where(a => a.A == v || a.B == v))
                {
                    int u = eG.getNeighbour(v);
                    if (!visited[u])
                    {
                        S.Push(u);
                        S.Push(v);
                        visited[u] = true;
                        continue;
                    }
                    if (u != w) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Looks for the proper net based on the graph
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="netsManager"></param>
        /// <returns></returns>
        public static int getNetOfTheGraph(Graph graph, NetsManager netsManager)
        {
            Graph resultGraph = null;
            var longestPath = graph.getLongestPath();
            var shortestPath = graph.getShortestPath();
            var netss = (netsManager.nets.FindAll(g => g.getLongestPath() == longestPath).FindAll(gr=>gr.getShortestPath()==shortestPath));
            var nets = netss.FindAll(gr => gr.leavesAmount == graph.leavesAmount);

            foreach(var net in nets)
            {
                List<int> gList = new List<int>() { graph.maxX, graph.maxY, graph.maxZ };
                if (!gList.Remove(net.maxX))
                {
                    continue;
                }
                if (!gList.Remove(net.maxY))
                {
                    continue;
                }
                if (!gList.Remove(net.maxZ))
                {
                    continue;
                }
                resultGraph = net;
                break;
            }
            if (resultGraph == null)
            {
                throw new Exception("Graph not found!");
            }
            return netsManager.nets.IndexOf(resultGraph);
        }
    }
}
