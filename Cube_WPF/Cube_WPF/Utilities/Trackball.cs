﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using System.Windows.Markup;
using System.Diagnostics.Tracing;
using System.Windows.Media;

namespace Cube_WPF
{
    /// <summary>
    /// Class used for camera rotation
    /// </summary>
    public static class Trackball
    {
        public static double _rotationFactor;

        /// <summary>
        /// Gets the current orientantion from the RotateTransform3D 
        /// and writes the new orientation to the Rotation3D
        /// 
        /// </summary>
        /// <param name="currentPosition"></param>
        /// <param name="eventSource"></param>
        /// <param name="_previousPosition3D"></param>
        /// <param name="_rotation"></param>
        public static void Track(Point currentPosition, FrameworkElement eventSource, ref Vector3D _previousPosition3D, ref AxisAngleRotation3D _rotation)
        {
            //Vector3D currentPosition3D = ProjectToTrackball(
            //    eventSource.ActualWidth, eventSource.ActualHeight, currentPosition);

            //Vector3D axis = Vector3D.CrossProduct(_previousPosition3D, currentPosition3D);
            //double angle = Vector3D.AngleBetween(_previousPosition3D, currentPosition3D);
            //Quaternion delta = new Quaternion(axis, -angle);

            //AxisAngleRotation3D r = _rotation;
            //Quaternion q = new Quaternion(_rotation.Axis, _rotation.Angle);

            Vector3D currentPosition3D = ProjectToTrackball(
                eventSource.ActualWidth, eventSource.ActualHeight, currentPosition);

            if (_previousPosition3D.Equals(currentPosition3D)) return;

            var axis = Vector3D.CrossProduct(_previousPosition3D, currentPosition3D);

            var angle = _rotationFactor * Vector3D.AngleBetween(_previousPosition3D, currentPosition3D);
            var delta = new Quaternion(axis, -angle);

            // Get the current orientation from the RotateTransform3D
            var q = new Quaternion(_rotation.Axis, _rotation.Angle);

            // Compose the delta with the previous orientation
            q *= delta;

            // Write the new orientation back to the Rotation3D
            _rotation.Axis = q.Axis;
            _rotation.Angle = q.Angle;

            _previousPosition3D = currentPosition3D;
        }

        public static Vector3D ProjectToTrackball(double width, double height, Point point)
        {
            double x = point.X / (width / 2);    // Scale so bounds map to [0,0] - [2,2]
            double y = point.Y / (height / 2);

            x = x - 1;                           // Translate 0,0 to the center
            y = 1 - y;                           // Flip so +Y is up instead of down

            double z2 = 1 - x * x - y * y;       // z^2 = 1 - x^2 - y^2
            double z = z2 > 0 ? Math.Sqrt(z2) : 0;

            return new Vector3D(x, y, z);
        }
        public static Vector3D VectorFromPoint(Point3D p)
        {
            return new Vector3D(p.X, p.Y, p.Z);
        }

        public static Point3D PointFromVector(Vector3D v)
        {
            return new Point3D(v.X, v.Y, v.Z);
        }


    }
}

