﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace Cube_WPF.Utilities
{
    /// <summary>
    /// Information about a net allowing visualisation
    /// </summary>
    public class NetViewer
    {
        /// <summary>
        /// 2D points
        /// </summary>
        public List<Point> points { get; set; }
        /// <summary>
        /// 3D functionalities
        /// </summary>
        public List<NetFace> faces { get; set; }

        /// <summary>
        /// Calculates the 3D points of the net based on 2D points
        /// </summary>
        /// <returns></returns>
        public List<NetFace> getNetFaces()
        {
            faces = new List<NetFace>();
            foreach(var point in points)
            {
                faces.Add(new NetFace()
                {
                    Positions = new Point3DCollection { new Point3D(-0.5 + point.X, -0.5 + point.Y, 0), new Point3D(0.5 + point.X, -0.5 + point.Y, 0), new Point3D(0.5 + point.X, 0.5 + point.Y, 0), new Point3D(-0.5 + point.X, 0.5 + point.Y, 0) },
                    Indices = new Int32Collection { 1, 0, 2, 3, 2, 0 },
                    Color = ViewerManager.cubeColor
                });
            }
            return faces;          
        }
    }
    /// <summary>
    /// 3D functionalities of a single face of the net
    /// </summary>
    public class NetFace
    {
        /// <summary>
        /// Positions of wall - list of 3D points to allow visualisation
        /// </summary>
        public Point3DCollection Positions { get; set; }
        /// <summary>
        /// Indices to support visualisation 
        /// (contains information about triangles)
        /// </summary>
        public Int32Collection Indices { get; set; }
        /// <summary>
        /// Color of face, supports visualisation
        /// </summary>
        public Brush Color { get; set; }
    }
}
