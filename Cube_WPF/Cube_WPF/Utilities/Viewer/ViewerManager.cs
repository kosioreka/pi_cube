﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Cube_WPF.Utilities
{
    /// <summary>
    /// Manager for properties of the visualization
    /// </summary>
    public static class ViewerManager
    {
        /// <summary>
        /// The color of the cube
        /// </summary>
        public static Brush cubeColor { get; set; }
    }
}
