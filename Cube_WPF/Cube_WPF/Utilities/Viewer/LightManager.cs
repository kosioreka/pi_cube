﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace Cube_WPF.Utilities.Viewer
{
    /// <summary>
    /// Manager for handling light of the scene
    /// </summary>
    public class LightManager
    {
        /// <summary>
        /// Type of light currently used
        /// </summary>
        public LightType Type;
        /// <summary>
        /// Default value is Multilight
        /// </summary>
        public LightManager()
        {
            Type = LightType.Multilight;
        }
        /// <summary>
        /// Returns the list of lights to be given as 3D objects to the ViewPort
        /// </summary>
        /// <returns></returns>
        public List<Light> getLights()
        {
            List<Light> result = null;
            switch (Type)
            {
                case LightType.AmbientLight:
                    return setAmbient();
                case LightType.DirectionalLight:
                    return setDirectional();
                case LightType.Multilight:
                    return setMulti();
            }
                return result;
        }
        /// <summary>
        /// Retruns ambient light
        /// </summary>
        /// <returns></returns>
        public List<Light> setAmbient()
        {
            var lights = new List<Light>
            {
                new AmbientLight
                {
                    Color = Colors.White
                }
            };
            return lights;
        }
        /// <summary>
        /// Returns directional light
        /// </summary>
        /// <returns></returns>
        public List<Light> setDirectional()
        {
            var lights = new List<Light>
            {
                new PointLight { Position = new Point3D(0.6, 0.6, 0.6) },
                new PointLight { Position = new Point3D(0.6, 0.6, -0.6) },
                new PointLight { Position = new Point3D(0.6, -0.6, 0.6) },
                new PointLight { Position = new Point3D(0.6, -0.6, -0.6) },
            };

            lights.ForEach(l => l.Color = Colors.White);
            return lights;
        }
        /// <summary>
        /// Returns light pointed to many points
        /// </summary>
        /// <returns></returns>
        public List<Light> setMulti()
        {
            var lights = new List<Light>
            {
                new SpotLight
                {
                    Color = Colors.White,
                    Position = new Point3D(-2, -2, -2),
                    Direction = new Vector3D(2, 2, 2)
                },
                new SpotLight
                {
                    Color = Colors.White,
                    Position = new Point3D(-2, -2, 2),
                    Direction = new Vector3D(2, 2, -2)
                },
                new SpotLight
                {
                    Color = Colors.White,
                    Position = new Point3D(2, 2, 2),
                    Direction = new Vector3D(-2, -2, -2)
                },
                new SpotLight
                {
                    Color = Colors.White,
                    Position = new Point3D(2, 2, 0),
                    Direction = new Vector3D(-2, -2, 2)
                }
            };

            return lights;
        }

    }
    /// <summary>
    /// Possible lights used for the scene
    /// </summary>
    public enum LightType{
        DirectionalLight =1,
        Multilight=2,
        AmbientLight=3
    }
}
