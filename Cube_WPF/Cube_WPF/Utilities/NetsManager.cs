﻿using Cube_WPF.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cube_WPF
{
    /// <summary>
    /// Manager for nets
    /// Generates logic for all nets upon starting the program
    /// </summary>
    public class NetsManager
    {
        private Graph fullGraph;
        /// <summary>
        /// Graphs  that visualize the logic for nets
        /// </summary>
        public List<Graph> nets = new List<Graph>();
        /// <summary>
        /// NetViewers equivalent to the graphs (nets)
        /// </summary>
        public List<NetViewer> netsViewers = new List<NetViewer>();

        /// <summary>
        /// Cnstractor generates 11 possible nets
        /// Calculates leaves and paths in the nets
        /// </summary>
        /// <param name="cube">Used for getting faces of the cube</param>
        public NetsManager(Cube cube)
        {
            this.fullGraph = new Graph(cube);

            
            ///     0
            ///   2 1 4
            ///     5
            ///     3
            nets.Add(new Graph()
            {
                edgeGraphs = new List<EdgeGraph>() {
                    new EdgeGraph(0, 1, fullGraph.getIndexEdge(0,1)),
                new EdgeGraph(1, 5, fullGraph.getIndexEdge(1,5)),
                new EdgeGraph(5, 3, fullGraph.getIndexEdge(5,3)),
                new EdgeGraph(2, 1, fullGraph.getIndexEdge(2,1)),
                new EdgeGraph(1, 4, fullGraph.getIndexEdge(1,4))
                }
            });
            netsViewers.Add(new NetViewer() { points = new List<Point> { new Point(0, 1), new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(0, -1) }, });

            ///   2 0 4
            ///     1
            ///     5
            ///     3
            nets.Add(new Graph()
            {
                edgeGraphs = new List<EdgeGraph>() {
                    new EdgeGraph(0, 1, fullGraph.getIndexEdge(0,1)),
                new EdgeGraph(1, 5, fullGraph.getIndexEdge(1,5)),
                new EdgeGraph(5, 3, fullGraph.getIndexEdge(5,3)),
                new EdgeGraph(2, 0, fullGraph.getIndexEdge(2,0)),
                new EdgeGraph(0, 4, fullGraph.getIndexEdge(0,4))
                }
            });
            netsViewers.Add(new NetViewer(){ points = new List<Point> { new Point(-1, 1), new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(-1, -1) }});

            ///         2
            ///     0 1 5 3
            ///           4
            nets.Add(new Graph()
            {
                edgeGraphs = new List<EdgeGraph>() {
                    new EdgeGraph(0, 1, fullGraph.getIndexEdge(0,1)),
                new EdgeGraph(1, 5, fullGraph.getIndexEdge(1,5)),
                new EdgeGraph(5, 3, fullGraph.getIndexEdge(5,3)),
                new EdgeGraph(2, 5, fullGraph.getIndexEdge(2,5)),
                new EdgeGraph(3, 4, fullGraph.getIndexEdge(3,4))
                }
            });
            netsViewers.Add(new NetViewer() { points = new List<Point> { new Point(-1, 1), new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(0, -1) },});

            ///       2  
            ///     0 1 5 3
            ///           4
            nets.Add(new Graph()
            {
                edgeGraphs = new List<EdgeGraph>() {
                    new EdgeGraph(0, 1, fullGraph.getIndexEdge(0,1)),
                new EdgeGraph(1, 5, fullGraph.getIndexEdge(1,5)),
                new EdgeGraph(5, 3, fullGraph.getIndexEdge(5,3)),
                new EdgeGraph(2, 1, fullGraph.getIndexEdge(2,1)),
                new EdgeGraph(3, 4, fullGraph.getIndexEdge(3,4))
                }
            });
            netsViewers.Add(new NetViewer()
            {
                points = new List<Point> { new Point(-1, 1), new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(1, -1) },
            });

            ///     2
            ///     0 1 5 3
            ///           4
            nets.Add(new Graph()
            {
                edgeGraphs = new List<EdgeGraph>() {
                    new EdgeGraph(0, 1, fullGraph.getIndexEdge(0,1)),
                new EdgeGraph(1, 5, fullGraph.getIndexEdge(1,5)),
                new EdgeGraph(5, 3, fullGraph.getIndexEdge(5,3)),
                new EdgeGraph(2, 0, fullGraph.getIndexEdge(2,0)),
                new EdgeGraph(3, 4, fullGraph.getIndexEdge(3,4))
                }
            });
            netsViewers.Add(new NetViewer()
            {
                points = new List<Point> { new Point(-1, 1), new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(2, -1) },
            });
        
            ///       2
            ///     0 1 5 3
            ///         4
            nets.Add(new Graph()
            {
                edgeGraphs = new List<EdgeGraph>() {
                    new EdgeGraph(0, 1, fullGraph.getIndexEdge(0,1)),
                new EdgeGraph(1, 5, fullGraph.getIndexEdge(1,5)),
                new EdgeGraph(5, 3, fullGraph.getIndexEdge(5,3)),
                new EdgeGraph(2, 1, fullGraph.getIndexEdge(2,1)),
                new EdgeGraph(5, 4, fullGraph.getIndexEdge(5,4))
                }
            });
            netsViewers.Add(new NetViewer()
            {
                points = new List<Point> { new Point(0, 1), new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(1, -1) },
            });

            ///     2
            ///     0 1 5
            ///         4 3
            nets.Add(new Graph()
            {
                edgeGraphs = new List<EdgeGraph>() {
                    new EdgeGraph(0, 1, fullGraph.getIndexEdge(0,1)),
                new EdgeGraph(1, 5, fullGraph.getIndexEdge(1,5)),
                new EdgeGraph(5, 4, fullGraph.getIndexEdge(5,4)),
                new EdgeGraph(2, 0, fullGraph.getIndexEdge(2,0)),
                new EdgeGraph(3, 4, fullGraph.getIndexEdge(3,4))
                }
            });
            netsViewers.Add(new NetViewer()
            {
                points = new List<Point> { new Point(-1, 1), new Point(0, 1), new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(2, -1) },
            });

            ///     0 1 5
            ///         4 3 2
            nets.Add(new Graph()
            {
                edgeGraphs = new List<EdgeGraph>() {
                    new EdgeGraph(0, 1, fullGraph.getIndexEdge(0,1)),
                new EdgeGraph(1, 5, fullGraph.getIndexEdge(1,5)),
                new EdgeGraph(5, 4, fullGraph.getIndexEdge(5,4)),
                new EdgeGraph(3, 4, fullGraph.getIndexEdge(3,4)),
                new EdgeGraph(3,2, fullGraph.getIndexEdge(3,2))
                }
            });
            netsViewers.Add(new NetViewer()
            {
                points = new List<Point> { new Point(-2, 1), new Point(-1, 1), new Point(0, 1), new Point(0, 0), new Point(1, 0), new Point(2, 0) },

            });

            ///         2
            ///     0 1 5 
            ///         4 3
            nets.Add(new Graph()
            {
                edgeGraphs = new List<EdgeGraph>() {
                    new EdgeGraph(0, 1, fullGraph.getIndexEdge(0,1)),
                new EdgeGraph(1, 5, fullGraph.getIndexEdge(1,5)),
                new EdgeGraph(5, 4, fullGraph.getIndexEdge(5,4)),
                new EdgeGraph(2, 5, fullGraph.getIndexEdge(2,5)),
                new EdgeGraph(3, 4, fullGraph.getIndexEdge(3,4))
                }
            });
            netsViewers.Add(new NetViewer()
            {
                points = new List<Point> { new Point(0, 0), new Point(-2, 0), new Point(-1, 0), new Point(0, 0), new Point(0, -1), new Point(1, -1) },
            });

            ///   5 2
            ///     1 0
            ///       4 3
            nets.Add(new Graph()
            {
                edgeGraphs = new List<EdgeGraph>() {
                    new EdgeGraph(0, 1, fullGraph.getIndexEdge(0,1)),
                new EdgeGraph(2, 5, fullGraph.getIndexEdge(1,5)),
                new EdgeGraph(4, 0, fullGraph.getIndexEdge(5,3)),
                new EdgeGraph(2, 1, fullGraph.getIndexEdge(2,0)),
                new EdgeGraph(3, 4, fullGraph.getIndexEdge(3,4))
                }
            });
            netsViewers.Add(new NetViewer()
            {
                points = new List<Point> { new Point(-1, 1), new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(1, -1), new Point(2, -1) },
            });

            ///     2
            ///   1 0 3
            ///       4 5
            nets.Add(new Graph()
            {
                edgeGraphs = new List<EdgeGraph>() {
                    new EdgeGraph(0, 1, fullGraph.getIndexEdge(0,1)),
                new EdgeGraph(2, 0, fullGraph.getIndexEdge(2,0)),
                new EdgeGraph(0, 3, fullGraph.getIndexEdge(0,3)),
                new EdgeGraph(4, 5, fullGraph.getIndexEdge(4,5)),
                new EdgeGraph(3, 4, fullGraph.getIndexEdge(3,4))
                }
            });
            netsViewers.Add(new NetViewer()
            {
                points = new List<Point> { new Point(0, 1), new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(1, -1), new Point(2, -1) },
            });


            foreach (var net in nets)
            {
                net.faceGraphs = fullGraph.faceGraphs;
                net.setNeighburs();
                net.calculateLongestPath();
            }
        }
       

    }

}
